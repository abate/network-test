#!/bin/sh

rm -Rf /home/tezos/.tezos-node
rm -Rf /home/tezos/node
mkdir /home/tezos/node
chown tezos. /home/tezos/node

/usr/local/bin/tezos-node config \
  --data-dir=/home/tezos/node \
  --rpc-addr 0.0.0.0 \
  --net-addr 0.0.0.0:9732 \
  --no-bootstrap-peers \
  --expected-pow=2.0 \
  init

/usr/local/bin/tezos-node identity --data-dir=/home/tezos/node generate 3.

/usr/local/bin/tezos-client \
  import secret key bootstrap1 unencrypted:edsk3gUfUPyBSfrS9CCgmCiQsTCHGkviBDusMxDJstFtojtc1zcpsh

/usr/local/bin/tezos-client \
  import secret key bootstrap2 unencrypted:edsk39qAm1fiMjgmPkw1EgQYkMzkJezLNewd7PLNHTkr6w9XA2zdfo

/usr/local/bin/tezos-client \
  import secret key bootstrap3 unencrypted:edsk4ArLQgBTLWG5FJmnGnT689VKoqhXwmDPBuGx3z4cvwU9MmrPZZ

/usr/local/bin/tezos-client \
  import secret key bootstrap4 unencrypted:edsk2uqQB9AY4FvioK2YMdfmyMrer5R8mGFyuaLLFfSRo8EoyNdht3

/usr/local/bin/tezos-client \
  import secret key bootstrap5 unencrypted:edsk4QLrcijEffxV31gGdN2HU7UpyJjA8drFoNcmnB28n89YjPNRFm

/usr/local/bin/tezos-client \
  import secret key activator unencrypted:edsk31vznjHSSpGExDMHYASz45VZqXN4DPxvsa4hAyY8dHM28cZzp6

/usr/local/bin/tezos-node run \
  --data-dir=/home/tezos/node \
  --network=sandbox \
  --sandbox=/sandbox.json \
  --singleprocess
