#!/bin/sh

rm -Rf /home/tezos/.tezos-node
rm -Rf /home/tezos/node
mkdir /home/tezos/node
chown tezos. /home/tezos/node

/usr/local/bin/tezos-node config \
  --data-dir=/home/tezos/node \
  --net-addr 0.0.0.0:9732 \
  --no-bootstrap-peers \
  --expected-pow=2.0 \
  --discovery-addr=255.255.255.255:9999 \
  init

/usr/local/bin/tezos-node identity --data-dir=/home/tezos/node generate 3.

/usr/local/bin/tezos-node run \
  --singleprocess \
  --data-dir=/home/tezos/node \
  --sandbox=/sandbox.json \
  --network=sandbox
